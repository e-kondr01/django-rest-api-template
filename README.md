# Django REST API template

## Local environment
### Installation
#### Django project
1. Install Python 3.12

2. Install poetry >=1.8:

https://python-poetry.org/docs/#installation 


3. Choose python3.12 for Poetry

https://python-poetry.org/docs/managing-environments/#switching-between-environments

```bash
cd template_project

poetry env use 3.12
```

4. Install requirements

```bash
poetry install --with dev
```

Pin packages versions after initializing the project.

5. Copy .env
```bash
cp template_project/local.example.env template_project/.env
```

#### Docker
Refer to:

https://docs.docker.com/engine/install/

### Usage

1. Start PSQL in Docker Compose:
```bash
docker compose -f local-docker-compose.yml up -d
```

2. Apply migrations:
```bash
cd template_project
poetry run python manage.py migrate
```

3. Run development server:
```bash
poetry run python manage.py runserver
```

OR use a shortcut script:

```bash
make local
```

### Migrations

Create a migration:
```bash
cd template_project
poetry run python manage.py makemigrations
```

Migrate:
```bash
poetry run python manage.py migrate
```

### Linting and formatting
Use `ruff` with config from `template_project/pyproject.toml`:

Lint:
```bash
poetry run ruff check .

poetry run mypy .
```

Format:
```bash
poetry run ruff format .
```

## API Docs
Swagger UI is available at `GET /api/docs` when the server is running.

## Production environment

### Installation
1. Copy .env:
```bash
cp template_project/production.example.env template_project/.env
```

2. Don't forget to set env variables such as `ALLOWED_HOSTS`, `CSRF_TRUSTED_ORIGINS`, `CORS_ALLOWED_ORIGINS`
and `SECRET_KEY`.

### Usage

```bash
make up
```

Collect static files:

```bash
docker exec -ti template_project_backend python manage.py collectstatic
```
