up:
	docker compose up --build

run: up

deploy:
	git pull
	docker compose up --build --force-recreate -d

local:
	docker compose -f local-docker-compose.yml up -d
	cd template_project; poetry run python manage.py migrate; \
	poetry run python manage.py runserver

migrations:
	cd template_project; poetry run python manage.py makemigrations

migrate:
	cd template_project; poetry run python manage.py migrate

lint:
	cd template_project; poetry run ruff check . ; poetry run mypy .

format:
	cd template_project; poetry run ruff format .

.DEFAULT_GOAL := local
